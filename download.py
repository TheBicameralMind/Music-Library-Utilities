import os
import json
import shutil
import base64
import youtube_dl
from mutagen.mp3 import MP3
from mutagen.easyid3 import EasyID3
from datetime import datetime, timedelta

DL_DIR = 'tmp/'
CL_DIR = 'collections/'
delimiter = '\{delimit\}'

ydl_opts = {
    'format': 'bestaudio/best',
    'postprocessors': [{
        'key': 'FFmpegExtractAudio',
        'preferredcodec': 'mp3',
        'preferredquality': '192',
    }],
    'outtmpl' : DL_DIR + '/%(creator)s{}%(title)s.%(ext)s'.format(delimiter), 
    'no-warnings' : True,
    'nocheckcertificate' : True,
    # 'quiet' : True, 
    # 'forcetitle' : True
    # 'progress_hooks': [dl_hook],
}

def get_playlists():
    with open('playlists.json') as f:
        pl = json.load(f)
        f.close()
        return pl

def md_open(col_name):
	"""Open a collection's metadata"""
	with open("metadata/md_" + col_name + ".json") as f:
		meta = json.load(f)
		f.close()
		return meta

def md_close(col_name, meta):
	"""Close a collection's metadata"""
	with open("metadata/md_" + col_name + ".json", 'w') as f:
		json.dump(meta, f)
		f.close()

def to_b64(st):
    return str(base64.b64encode(st[:-4].encode()))[2:-1]

# def b64d(st):
	# return str(base64.b64decode(st))[2:-1]

# def dl_hook(d):
#     if d['status'] == 'downloading': 
#         print("")

def download_playlist(col, url):
    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        ydl.download(url)

    for f in os.listdir(DL_DIR):
        f_cln = clean_name(f)
        tag_media(f_cln)
        f_enc = encode_name(f_cln)
        copy_to_col('UmVjZW50bHkgQWRkZWQ=', f_enc)
        add_metadata('UmVjZW50bHkgQWRkZWQ=', f_enc)
        move_to_col(col, f_enc)
        add_metadata(col, f_enc)

def clean_name(fn):
    filter = ["(Original Mix)", "[Monstercat Release]", "[NCS Release]", "(lyric video)", "(official audio)",
                 "(official video)", "[Tasty Release]", "_Creative Commons_", "(Creative Commons)", "(Audio)",
                 "(audio)", "[Lyric Video]", "(lyrics)", "(Lyrics)", "[NCS Release]", "(Official Music Video)"
                 "Lyrics", "(Official Audio)", "[Audio]", "(Official audio)", "[Electro Swing]"]
    t_fn = fn
    for phrase in filter: 
        if phrase in t_fn:
            t_fn = t_fn.replace(phrase, "").replace(" .mp3", ".mp3").strip()

    channel_name, video_title = t_fn.split(delimiter)
    if ' - ' in video_title: 
        t_fn = video_title
    else: 
        t_fn = t_fn.replace(delimiter, ' - ')

    os.rename(DL_DIR + fn, DL_DIR + t_fn)
    print("Renamed {}".format(t_fn))
    return t_fn

def tag_media(fn):
    try: 
        split = fn.find(' - ')
        artist = fn[:split]
        title = fn[split + 3:-4]
        audio = EasyID3(DL_DIR + fn)
        audio["title"] = title
        audio["artist"] = artist
        audio["album"] = ""
        audio.save()
        print("Tagged {}".format(fn))
    except Exception as e:
        print('Exception {} tagging song {}'.format(e.message, fn)) 

def encode_name(fn):
    n_name = to_b64(fn) + '.mp3'
    os.rename(DL_DIR + fn, DL_DIR + n_name)
    print("Encoded {} as {}".format(fn, n_name))
    return n_name

def move_to_col(col_name, fn):
    # for fn in os.listdir(DL_DIR):
    try:
        os.rename(DL_DIR + fn, CL_DIR + col_name + '/' + fn)
        print("Moved {} to {}".format(fn, col_name))
    except Exception as e:
        print('Exception {} moving song {}'.format(e.message, fn))

def copy_to_col(col_name, fn):
    try:
        shutil.copy2(DL_DIR + fn, CL_DIR + col_name + '/' + fn)
        print("Copied {} to {}".format(fn, col_name))
    except Exception as e:
        print('Exception {} moving song {}'.format(e.message, fn))

def clean_recent():
    md_close('UmVjZW50bHkgQWRkZWQ=', {})
    try: 
        shutil.rmtree(CL_DIR + 'UmVjZW50bHkgQWRkZWQ=')
    except Exception as e: 
        print("Unable to remove recent directory: {}".format(e))
    os.mkdir(CL_DIR + 'UmVjZW50bHkgQWRkZWQ=')
    

def add_metadata(col, name):
    mp3 = EasyID3(CL_DIR + col + '/' + name)
    title = mp3['title'][0]
    artist = mp3['artist'][0]
    rawtime = MP3(CL_DIR + col + "/" + name).info.length
    runtime = str(timedelta(seconds=round(rawtime)))
    code = name[:-4]
    meta = md_open(col)
    meta[code] = {"title" : title, "artist" : artist, "runtime" : runtime}
    md_close(col, meta)
    print("Added metadata for {} to {}".format(name, col))


def main():
    playlists = get_playlists()

    clean_recent()

    for collection, url in playlists.items():
        download_playlist(collection, url)
    
if __name__ == '__main__':
    main()
