import os
import base64
from mutagen.easyid3 import EasyID3

for f in os.listdir('files'):
    mp3 = EasyID3('files/{}'.format(f))
    title = mp3['title'][0]
    artist = mp3['artist'][0]
    name = str(base64.b64encode(f[:-4].encode()))[2:-1]
    os.rename('files/{}'.format(f), 'files/{}.mp3'.format(name))