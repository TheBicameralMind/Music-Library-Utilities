import os
import base64
import json
import click
import shutil
from mutagen.easyid3 import EasyID3
from fuzzywuzzy import process

# utility functions
def md_open(col_name):
	"""Open a collection's metadata"""
	with open("metadata/md_" + col_name + ".json") as f:
		meta = json.load(f)
		f.close()
		return meta

def md_close(col_name, meta):
	"""Close a collection's metadata"""
	with open("metadata/md_" + col_name + ".json", 'w') as f:
		json.dump(meta, f)
		f.close()

def b64e(st):
	"""Encode a string using Base64 and return the string"""
	# turn str(base64.b64encode(st.encode()))[2:-1]
	return base64.b64encode(st.encode()).decode()

def b64d(st):
	"""Decode a string using Base64 and return the string"""
	# return str(base64.b64decode(st))[2:-1]
	return base64.b64decode(st.encode()).decode()

@click.group()
def main():
	pass

@main.command()
@click.option('-cs', '--collection-search', 'col_s', prompt='Collection title best guess')
def col_search(col_s):
	"""Search for a particular collection"""
	col_meta = md_open('collections')
	options = [col_meta[v] for v in col_meta]
	matches = process.extract(col_s, options, limit=9)

	if len(matches) == 0:
		click.echo("No collection matches found")
		return None

	c = 1
	for m in matches:
		click.echo(str(c) + ": " + m[0] + ", " + str(m[1]) + '%')
		c += 1

	md_close('collections', col_meta)

	return matches

@main.command()
@click.option('-cn', '--collection-name', 'col_name', prompt='Title for collection')
def new_col(col_name):
	"""Create a new collection"""
	col_meta = md_open('collections')
	col_enc = b64e(col_name)

	if col_name in col_meta.values():
		click.echo("Collection already exists.")
		return

	if os.path.exists('collections' + col_enc):
		try:
			shutil.rmtree('collections' + col_enc)
		except OSError as e:
			print('Error: {} - {}'.format(e.filename, e.strerror))

	col_meta[col_enc] = col_name
	os.mkdir('collections/' + col_enc)

	md_close(col_enc, {})
	md_close('collections', col_meta)

@main.command()
@click.option('-cn', '--collection-name', 'col_name', prompt='Collection title best guess')
@click.pass_context
def delete_col(ctx, col_name):
	"""Delete a current collection (and all it's [meta]data)"""
	matches = ctx.invoke(col_search, col_s=col_name)

	if not matches: return

	picked = int(input("Choose 1-9 for your choice: "))
	if picked > 9 or picked < 1:
		click.echo("Number out of range.")
		return

	match = matches[picked - 1][0]

	delete = input('Delete "{}" and all enclosed songs? '.format(match))
	if delete.lower() != 'y': return

	col_meta = md_open('collections')
	del col_meta[b64e(match)]
	try:
		shutil.rmtree('collections/' + b64e(match))
		os.remove('metadata/md_' + b64e(match) + '.json')
	except OSError as e:
		print('Error: {} - {}'.format(e.filename, e.strerror))
		return

	click.echo('Successfully deleted  collection: {}'.format(match))

	md_close('collections', col_meta)

@main.command()
def p_col():
	"""List all current collections"""
	col_meta = md_open('collections')
	click.echo('All collections: ')
	for v in col_meta:
		click.echo('"{}" ("{}")'.format(col_meta[v], v))

@main.command()
@click.option('-sc', '--search-collection', 'coll', prompt='Collection title best guess')
@click.option('-cs', '--collection-search', 'kw', prompt='Song title best guess')
@click.pass_context
def search_col(ctx, coll, kw):
	"""Search within a collection for a song"""
	matches = ctx.invoke(col_search, col_s=coll)

	if not matches: return

	picked = int(input("Choose 1-9 for your choice: "))
	if picked > 9 or picked < 1:
		click.echo("Number out of range.")
		return

	match = matches[picked - 1]

	song_meta = md_open(b64e(match[0]))

	options = [b64d(o) for o in song_meta]
	matches = process.extract(kw, options, limit=9)

	if len(matches) == 0:
                click.echo("No song matches found.")
                return None
	c = 1
	for m in matches:
			click.echo(str(c) + ": " + m[0] + ", " + str(m[1]) + '%')
			c += 1

	return {'col': match[0], 'mat': matches}

@main.command()
@click.option('-cn', '--collection-name', 'col_name', prompt='Collection title best guess')
@click.option('-sn', '--song-name', 'song_name', prompt='Song title best guess')
@click.pass_context
def rename(ctx, col_name, song_name):
	"""Rename a song within a collection"""
	res = ctx.invoke(search_col, coll=col_name, kw=song_name)
	col = b64e(res['col'])
	matches = res['mat']

	if not matches: return

	picked = int(input("Choose 1-9 for your choice: "))
	if picked > 9 or picked < 1:
		click.echo("Number out of range.")
		return

	match = matches[picked - 1][0]
	fb64 = b64e(match)
	fmatch = EasyID3('collections/{}/{}.mp3'.format(col, fb64))
	song_meta = md_open(col)

	click.echo("Old information: ")
	click.echo('    Title:\t"' + fmatch['title'][0] + '"')
	click.echo('    Artist:\t"' + fmatch['artist'][0] + '"')
	click.echo('    b64:\t"' + fb64 + '"')
	click.echo('------------------------------------------------------------')

	n_title = input("New Title:\t")
	n_artist = input("New Artist:\t")
	n_b64 = b64e(n_artist + ' - ' + n_title)

	click.echo('------------------------------------------------------------')
	click.echo("New information: ")
	click.echo('    Title:\t"' + n_title + '"')
	click.echo('    Artist:\t"' + n_artist + '"')
	click.echo('    b64:\t"' + n_b64 + '"')

	rename = input("Rename file and update JSON registry? (Y/N) ")
	if rename.lower() != 'y': return

	o = song_meta[fb64]
	o['title'] = n_title
	o['artist'] = n_artist
	del song_meta[fb64]
	song_meta[n_b64] = o

	os.rename('collections/{}/{}.mp3'.format(col, fb64), 'collections/{}/{}.mp3'.format(col, n_b64))

	click.echo('Renamed\n"{}" - "{}" ("{}") to\n"{}" - "{}" ("{}")'.format(fmatch['artist'][0], fmatch['title'][0], fb64, n_artist, n_title, n_b64))

	md_close(col, song_meta)

@main.command()
@click.option('-cn', '--collection-name', 'col_name', prompt='Collection title best guess')
@click.pass_context
def p_songs(ctx, col_name):
	"""List songs within a collection"""
	matches = ctx.invoke(col_search, col_s=col_name)

	if not matches: return

	picked = int(input("Choose 1-9 for your choice: "))
	if picked > 9 or picked < 1:
		click.echo("Number out of range.")
		return

	match = matches[picked - 1][0]

	song_meta = md_open(b64e(match))

	titles = []
	c = 0

	for j in song_meta:
		titles.append('"{}" - "{}" ("{}")'.format(song_meta[j]['artist'], song_meta[j]['title'], j))
		c += 1

	titles.sort()

	for k in titles:
		click.echo(k)
	click.echo('{} total songs'.format(c))

@main.command()
@click.option('-ccn', '--current-collection-name', 'c_col_name', prompt='Current collection title best guess')
@click.option('-sn', '--song-name', 'song_name', prompt='Song title best guess')
@click.option('-ncn', '--new-collection-name', 'n_col_name', prompt='New collection title best guess')
@click.pass_context
def move_song(ctx, c_col_name, song_name, n_col_name):
	"""Move a song from one existing collection to another"""
	c_info = ctx.invoke(search_col, coll=c_col_name, kw=song_name)

	c_col = c_info['col']
	c_matches = c_info['mat']

	if not c_matches: return

	picked = int(input("Choose 1-9 for your choice: "))
	if picked > 9 or picked < 1:
		click.echo("Number out of range.")
		return

	s_match = c_matches[picked - 1][0]

	n_matches = ctx.invoke(col_search, col_s=n_col_name)

	picked = int(input("Choose 1-9 for your choice: "))
	if picked > 9 or picked < 1:
		click.echo("Number out of range.")
		return

	n_col = n_matches[picked - 1][0]

	n_col_meta = md_open(b64e(n_col))
	c_col_meta = md_open(b64e(c_col))

	s_b64 = b64e(s_match)

	move = input('Move "{}" from collection "{}" to "{}"? (Y/N) '.format(s_match, c_col, n_col))
	if move.lower() != 'y': return

	n_col_meta[s_b64] = c_col_meta[s_b64]
	del c_col_meta[s_b64]
	os.rename('collections/{}/{}.mp3'.format(b64e(c_col), s_b64), 'collections/{}/{}.mp3'.format(b64e(n_col), s_b64))

	click.echo('Moved "{}" from collection "{}" to "{}"'.format(s_match, c_col, n_col))

	md_close(b64e(n_col), n_col_meta)
	md_close(b64e(c_col), c_col_meta)

@main.command()
@click.option('-ocn', '--old-collection-name', 'o_col_name', prompt='Old collection title best guess')
@click.option('-an', '--artist-name', 'artist_name', prompt='Artist name best guess')
@click.option('-ncn', '--new-collection-name', 'n_col_name', prompt='New collection title best guess')
@click.pass_context
def bulk_move(ctx, o_col_name, n_col_name, artist_name):
	"""Move all songs by a particular artist to a different collection"""
	c_info = ctx.invoke(search_col, coll=o_col_name, kw=artist_name)

	c_col = c_info['col']
	c_matches = c_info['mat']

	if not c_matches: return

	picked = int(input("Choose 1-9 for your choice: "))
	if picked > 9 or picked < 1:
		click.echo("Number out of range.")
		return

	a_match = c_matches[picked - 1][0]

	n_matches = ctx.invoke(col_search, col_s=n_col_name)

	picked = int(input("Choose 1-9 for your choice: "))
	if picked > 9 or picked < 1:
		click.echo("Number out of range.")
		return

	n_col = n_matches[picked - 1][0]

	n_col_meta = md_open(b64e(n_col))
	c_col_meta = md_open(b64e(c_col))

	t_artist = c_col_meta[b64e(a_match)]['artist']
	s_move = [a for a in c_col_meta.keys() if c_col_meta[a]['artist'] == t_artist]
	# for i in s_move: click.echo(i)
	click.echo('Move: ')
	for i in s_move:
		click.echo('  ' + b64d(i))
	move = input('From {} to {}? (Y/N) '.format(c_col, n_col))

	if move.lower() != 'y': return 

	for i in s_move:
		n_col_meta[i] = c_col_meta[i]
		del c_col_meta[i]
		os.rename('collections/{}/{}.mp3'.format(b64e(c_col), i), 'collections/{}/{}.mp3'.format(b64e(n_col), i))

		click.echo('Moved "{}" from collection "{}" to "{}"'.format(b64d(i), c_col, n_col))
	
	md_close(b64e(n_col), n_col_meta)
	md_close(b64e(c_col), c_col_meta)

if __name__ == '__main__':
	main()
