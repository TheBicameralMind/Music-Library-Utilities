import os
import base64
import json
from mutagen.easyid3 import EasyID3
from fuzzywuzzy import process

while True:
    cont = input("Continue? (Y/N) ")
    if cont.lower() != "y": break

    with open("musicDatabase.json") as f:
        songs = json.load(f)
        f.close()
    options = [str(base64.b64decode(o))[2:-1] for o in songs]

    to_rename = input("Enter the [artist] - [title] of the song you want to rename: ")
    matches = process.extract(to_rename, options, limit=9)

    if len(matches) == 0:
        print("No song matches found.")
        continue

    c = 1
    for m in matches:
        print(str(c) + ": " + m[0] + ", " + str(m[1]) + '%')
        c += 1
    picked = int(input("Choose 1-9 for your choice: "))
    if picked > 9 or picked < 1:
        print("Number out of range. ")
        continue

    match = matches[picked - 1]
    fb64 = str(base64.b64encode(match[0].encode()))[2:-1]
    fmatch = EasyID3('files/{}.mp3'.format(fb64))

    print("Old information: ")
    print("    Title:\t" + fmatch['title'][0])
    print("    Artist:\t" + fmatch['artist'][0])
    print("    b64:\t" + fb64)
    print('------------------------------------------------------------')

    n_title = input("New Title:\t")
    n_artist = input("New Artist:\t")
    n_b64 = str(base64.b64encode((n_artist + ' - ' + n_title).encode()))[2:-1]

    print('------------------------------------------------------------')
    print("New information: ")
    print("    Title:\t" + n_title)
    print("    Artist:\t" + n_artist)
    print("    b64:\t" + n_b64)

    rename = input("Rename file and update JSON registry? (Y/N) ")
    if rename.lower() != 'y': continue

    o = songs[fb64]
    o['title'] = n_title
    o['artist'] = n_artist
    del songs[fb64]
    songs[n_b64] = o

    with open("musicDatabase.json", 'w') as tf:
        json.dump(songs, tf)
        tf.close()

    os.rename('files/{}.mp3'.format(fb64), 'files/{}.mp3'.format(n_b64))

    print('Renamed {} - {} ({}) to {} - {} ({})'.format(fmatch['artist'][0], fmatch['title'][0], fb64, n_artist, n_title, n_b64))
