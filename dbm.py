import os
import base64
import json
import click
from mutagen.easyid3 import EasyID3
from fuzzywuzzy import process

def db_open():
	with open("musicDatabase.json") as f:
		songs = json.load(f)
		f.close()
		return songs

def rb_open():
	with open("recycleBin.json") as f:
		o_songs = json.load(f)
		f.close()
		return o_songs
	
def db_close(songs):
	with open("musicDatabase.json", 'w') as tf:
		json.dump(songs, tf)
		tf.close()

def rb_close(o_songs):
	with open("recycleBin.json", 'w') as tf:
		json.dump(o_songs, tf)
		tf.close()
		
@click.group()
def main():
	pass
		
@main.command()
@click.option('-s', '--search', prompt='Title best guess', help='Returns the 9 closest library matches to the search')
def search(search):
	songs = db_open()
	
	options = [str(base64.b64decode(o))[2:-1] for o in songs]
	matches = process.extract(search, options, limit=9)
	
	if len(matches) == 0:
		click.echo("No song matches found.")
		return None
	
	c = 1
	for m in matches:
		click.echo(str(c) + ": " + m[0] + ", " + str(m[1]) + '%')
		c += 1

	db_close(songs)
	
	return matches

@main.command()
@click.option('--search-recycle', prompt='Title best guess')
def search_recycle(search_recycle):
        songs = rb_open()

        options = [str(base64.b64decode(o))[2:-1] for o in songs]
        matches = process.extract(search_recycle, options, limit=9)

        if len(matches) == 0:
                click.echo("No song matches found.")
                return None

        c = 1
        for m in matches:
                click.echo(str(c) + ": " + m[0] + ", " + str(m[1]) + '%')
                c += 1
        rb_close(songs)

        return matches
        
        
@main.command()
@click.option('--rename', prompt='Title best guess')
@click.pass_context
def rename(ctx, rename):
	songs = db_open()
	
	matches = ctx.invoke(search, search=rename)
	
	if not matches:
		return
	
	picked = int(input("Choose 1-9 for your choice: "))
	if picked > 9 or picked < 1:
		click.echo("Number out of range.")
		return
	
	match = matches[picked - 1]
	fb64 = str(base64.b64encode(match[0].encode()))[2:-1]
	fmatch = EasyID3('files/{}.mp3'.format(fb64))

	click.echo("Old information: ")
	click.echo('    Title:\t"' + fmatch['title'][0] + '"')
	click.echo('    Artist:\t"' + fmatch['artist'][0] + '"')
	click.echo('    b64:\t"' + fb64 + '"')
	click.echo('------------------------------------------------------------')

	n_title = input("New Title:\t")
	n_artist = input("New Artist:\t")
	n_b64 = str(base64.b64encode((n_artist + ' - ' + n_title).encode()))[2:-1]

	click.echo('------------------------------------------------------------')
	click.echo("New information: ")
	click.echo('    Title:\t"' + n_title + '"')
	click.echo('    Artist:\t"' + n_artist + '"')
	click.echo('    b64:\t"' + n_b64 + '"')
	
	rename = input("Rename file and update JSON registry? (Y/N) ")
	if rename.lower() != 'y': return

	o = songs[fb64]
	o['title'] = n_title
	o['artist'] = n_artist
	del songs[fb64]
	songs[n_b64] = o
	
	os.rename('files/{}.mp3'.format(fb64), 'files/{}.mp3'.format(n_b64))
	
	click.echo('Renamed\n"{}" - "{}" ("{}") to\n"{}" - "{}" ("{}")'.format(fmatch['artist'][0], fmatch['title'][0], fb64, n_artist, n_title, n_b64))
	
	db_close(songs)

@main.command()
@click.option('--delete', prompt='Title best guess')
@click.pass_context
def delete(ctx, delete):
	songs = db_open()
	o_songs = rb_open()
	
	matches = ctx.invoke(search, search=delete)
	
	if not matches:
		return
	
	picked = int(input("Choose 1-9 for your choice: "))
	if picked > 9 or picked < 1:
		click.echo("Number out of range.")
		return
	
	match = matches[picked - 1]
	fb64 = str(base64.b64encode(match[0].encode()))[2:-1]
	fmatch = EasyID3('files/{}.mp3'.format(fb64))

	click.echo("Information: ")
	click.echo('    Title:\t"' + fmatch['title'][0] + '"')
	click.echo('    Artist:\t"' + fmatch['artist'][0] + '"')
	click.echo('    b64:\t"' + fb64)
	click.echo('------------------------------------------------------------')
	
	rename = input("Delete file and update JSON registry? (Y/N) ")
	if rename.lower() != 'y': return
	
	o_songs[fb64] = songs[fb64]
	del songs[fb64]
	
	os.rename('files/{}.mp3'.format(fb64), 'deleted/{}.mp3'.format(fb64))
	click.echo('Deleted "{}" - "{}" ("{}")'.format(fmatch['artist'][0], fmatch['title'][0], fb64))
	
	db_close(songs)
	rb_close(o_songs)

@main.command()
@click.option('--restore', prompt='Title best guess')
@click.pass_context
def restore(ctx, restore):
        songs = db_open()
        d_songs = rb_open()

        matches = ctx.invoke(search-recycle, search_recycle=restore)

        if not matches:
                return

        picked = int(input("Choose 1-9 for your choice: "))
        if picked > 9 or picked < 1:
                click.echo("Number out of range.")
                return

        match = matches[picked - 1]
        fb64 = str(base64.b64encode(match[0].encode()))[2:-1]
        fmatch = EasyID3('deleted/{}.mp3'.format(fb64))

        click.echo("Information: ")
        click.echo('    Title:\t"' + fmatch['title'][0] + '"')
        click.echo('    Artist:\t"' + fmatch['artist'][0] + '"')
        click.echo('    b64:\t"' + fb64 + '"')
        click.echo('------------------------------------------------------------')

        restore = input("Restore file and update JSON registry? (Y/N) ")
        if rename.lower() != 'y': return

        songs[fb64] = d_songs[fb64]
        del d_songs[fb64]

        os.rename('deleted/{}.mp3'.format(fb64), 'files/{}.mp3'.format(fb64))
        click.echo('Restored "{}" - "{}" ("{}")'.format(fmatch['artist'][0], fmatch['title'][0], fb64))

        db_close(songs)
        rb_close(d_songs)
        
@main.command()
def pall():
	songs = db_open()
	titles = []
	c = 0
	
	for j in songs:
		titles.append('"{}" - "{}" ("{}")'.format(songs[j]['artist'], songs[j]['title'], j))
		c += 1
	
	titles.sort()
	
	for k in titles:
		click.echo(k)
	
	click.echo("{} total songs".format(c))
	
	db_close(songs)
	
	
if __name__ == '__main__':
	main()
