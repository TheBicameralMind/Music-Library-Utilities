import json
import uuid
import os
import base64
from mutagen.easyid3 import EasyID3
from mutagen.mp3 import MP3
from datetime import datetime, timedelta

data = {}

if (os.path.isfile('musicDatabase.json')):
    with open('musicDatabase.json') as db:
        data = json.load(db)

total = 0
new = 0
for file in os.listdir("files"):
    mp3 = EasyID3("files/{}".format(file))
    title = mp3['title'][0]
    artist = mp3['artist'][0]
    total += 1
    rawtime = MP3("files/{}".format(file)).info.length
    inttime = timedelta(seconds=round(rawtime))
    runtime = str(inttime)
    code = file[:-4]
    if data.get(code, False):
        # print("<YES> {} - {}".format(artist, title))
        continue
    new += 1

    data[code] = {"title": title, "artist": artist, "runtime": runtime}
    print("ADDED {} - {}".format(artist, title))

with open('musicDatabase.json', 'w+') as db:
    json.dump(data, db)
    db.close()

print("Added {} new songs for a total of {} songs".format(new, total))
