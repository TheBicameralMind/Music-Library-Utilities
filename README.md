# Music Library Utilities
This is the collection of scrips I used to manage my music collection, before
the unfortunate loss of the entire library. The only two scripts I regularly
used towards the end were 
[`download.py`](https://gitlab.com/TheBicameralMind/Music-Library-Utilities/-/blob/master/download.py) 
and [`ndbm.py`](https://gitlab.com/TheBicameralMind/Music-Library-Utilities/-/blob/master/ndbm.py) 
-- everything else is from
earlier iterations and was deprecated fairly quickly. 